#!/bin/sh

# An entry point script is a script that's used to run our application inside docker.
# so we're going to define a simple shell script and we're going to do is 
# we are going to populate the default.conf.tpl so the config template with
#  the environment variables and then we are going to start our NGINX service. 
#  This is what docker is going to run when we run our container.

# this tells out Unix docker image that this is a shell script to be executed.
# next we're going to write 


set -e

# and this just says that during the execution of this script if any of the lines fail
# then return a failure and return the error to the screen. we do this because it helps
# when debugging and we want to know right away if there's any issues with our script
# so that we can fix it we don't want it to run some of the commands successfully 
# and then carry on and install our application but then really it didn't populate 
# the things properly and that might be hard to debug and waste a lot of time 
# so it's always best to use set -e if you must have all the commands run 
# successfully in the script.

# next we're going to use envsubst (evn substitute) to pass in the environment variables 
# to these values in our template (the ${} in our default.conf.tpl file) 
# we do this by typing


# we use the less than symbol to pass in the template file that we want to substitute 
# the environment variables for: < /etc/nginx/default.conf.tpl 
# We going to move our default.conf.tpl to the following location when we build our image 
# the location is going to be /etc/nginx/default.conf.tpl


# next we specify where the envsubst (evn substitute) command is going to output 
# the updated config. so it's going to take in the template, it's going to populate 
# the environment variables, and then it's going to output that to a new file so put > 
# and new file location will be: > /etc/nginx/conf.d/default.conf
# this is the default location where NGINX expects to find its configuration file.
 
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf


# The last thing we need to do in our entry point is to actually start our NGINX service. 
# The recommended way to start an NGINX service in docker is to do it as follows 


# what this does is it tells NGINX to run with the daemon off by default NGINX runs 
# as a background daemon service that means that when you start it, 
# it runs in the background and then when you stop it you need to stop the background task. 

# This is not great for docker because it's recommended that docker containers run 
# with the primary application in the foreground.

# So what we do here is we tell NGINX to run with the daemon off so that it runs in 
# the foreground of our docker execution, this way all of the logs and output from our
# NGINX server get printed to the docker output .so that's what we need to do for our
# entry point make sure we save the file and let's move on to the next lesson

nginx -g 'daemon off;'
