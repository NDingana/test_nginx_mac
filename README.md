# recipe-app-api-proxy

recipe app api proxy application


I'm going to add
here is called environment variables. these are the environment variables that
we're going to be using to configure our NGINX proxy.

the first one we're
going to add is called

LISTEN_PORT

and this is going to be the port to listen on
and it's going to default to 8000.

this is going to be
the port the NGINX server listens to connection from. We are setting it to
default to 8000 but you can make it custom to any port you want.

APP_HOST

this is the host
name of the app to forward requests to and this is going to default to a host
name called app. OK so this is going to be the host that the proxy forwards
requests to.

APP_PORT

finally we're going
to add a variable for the port that it forwards requests to.  so we're going to add a new variable called
app_ports and this will be the port of the app to forward requests to default
is going to be 9000.

OK so we're going to
be creating these variables and setting them later in the course so you'll see
how that's done but for now this is the definition of the proxy we're going to
create or the configuration items that are going to be available for the proxy
